<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->string('surname');
            $table->string('second_surname');
            $table->string('first_name');
            $table->string('other_name')->nullable();
            $table->string('email')->unique()->nullable();
            $table->date('date_of_admission');
            $table->boolean('state');
            $table->string('dni_number')->unique();
            $table->unsignedBigInteger('dniType_id');
            $table->foreign('dniType_id')->references('id')->on('dni_types');
            $table->unsignedBigInteger('area_id');
            $table->foreign('area_id')->references('id')->on('areas');
            $table->unsignedBigInteger('country_id');
            $table->foreign('country_id')->references('id')->on('countries');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
};
