<?php

namespace Database\Seeders;

use App\Models\Area;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class AreaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Area::create([
            'name' => 'Administración'
        ]);
        Area::create([
            'name' => 'Financiera'
        ]);
        Area::create([
            'name' => 'Compras'
        ]);
        Area::create([
            'name' => 'Infraestructura'
        ]);
        Area::create([
            'name' => 'Operación'
        ]);
        Area::create([
            'name' => 'Talento Humano'
        ]);
        Area::create([
            'name' => 'Servicios Varios'
        ]);
    }
}
