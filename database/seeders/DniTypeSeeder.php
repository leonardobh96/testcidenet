<?php

namespace Database\Seeders;

use App\Models\DniType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DniTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DniType::create([
            'name' => 'Cédula de Ciudadanía'
        ]);
        DniType::create([
            'name' => 'Cédula de Extranjería'
        ]);
        DniType::create([
            'name' => 'Pasaporte'
        ]);
        DniType::create([
            'name' => 'Permiso Especial'
        ]);
    }
}
