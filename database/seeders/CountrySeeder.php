<?php

namespace Database\Seeders;

use App\Models\Country;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Country::create([
            'name' => 'Colombia',
            'domain' => 'cidenet.com.co'
        ]);
        Country::create([
            'name' => 'Estados Unidos',
            'domain' => 'cidenet.com.us'
        ]);
    }
}
