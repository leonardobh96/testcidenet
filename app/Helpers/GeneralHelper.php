<?php
namespace app\Helpers;

use Carbon\Carbon;
use App\Models\Employee;
use Illuminate\Support\Facades\DB;


class GeneralHelper{

   public static function generateEmail($employee, $domain)
   {    
        $coincidences = Employee::where('first_name', $employee->first_name)
        ->where('surname', $employee->surname)->get();

        
        if(count($coincidences) == 1){
            $email = strtolower($employee->first_name)  . str_replace(" ", "", strtolower($employee->surname)) . "@" . $domain;
        }else{
            $email = strtolower($employee->first_name)  . str_replace(" ", "", strtolower($employee->surname)) . $employee->id ."@" . $domain;
        }
        return $email;
   }

   public static function validateDni($dni)
   {
        $employee = Employee::where('dni_number',$dni)->first();
        return ($employee) ? false : true;
   }
}