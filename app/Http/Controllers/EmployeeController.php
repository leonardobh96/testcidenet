<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\Employee;
use Illuminate\Http\Request;
use App\Helpers\GeneralHelper;
use App\Http\Requests\EmployeeRequest;
use App\Models\Area;
use App\Models\DniType;
use Illuminate\Support\Facades\Redirect;

class EmployeeController extends Controller
{

    private $employee;

    public function __construct(Employee $employee){
       $this->employee = $employee;
    }

    public function index()
    {
        $employees = Employee::whereHas('country')
                   ->with('area','country')
        ->get();

        return $employees;
    }

    public function store(EmployeeRequest $request)
    {
        
        $domain = Country::find($request['country_id']); 
        try {
            $employee = $this->employee::create([
                'surname' => $request['surname'],
                'second_surname' => $request['second_surname'],
                'first_name' => $request['first_name'],
                'other_name' => $request['other_name'],
                'date_of_admission' => $request['date_of_admission'],
                'state' => true,
                'dni_number' => $request['dni_number'],
                'dniType_id' => $request['dniType_id'],
                'area_id' => $request['area_id'],
                'country_id' => $request['country_id']
            ]);
            $employee->email =  GeneralHelper::generateEmail($employee, $domain->domain);
            $employee->save();
            return response("Empleado registrado exitosamente", 200);
        } catch (\Throwable $th) {
            return response("No se pudo registrar el empleado", 403);
        }
    }


    public function show(Employee $employee)
    {
        return $employee;
    }


    public function update(EmployeeRequest $request, Employee $employee)
    {
        $domain = Country::find($request['country_id']); 
        try {
            $employee->surname = $request['surname'];
            $employee->first_name = $request['first_name'];
            $employee->second_surname = $request['second_surname'];
            $employee->other_name = $request['other_name'];
            if($request['dni_number'] != $employee->dni_number){
                if(GeneralHelper::validateDni($request['dni_number'])){
                    $employee->dni_number = $request['dni_number'];
                }else{
                    return response("El Dni ". $request['dni_number'] . " ya se encuentra registrado", 403);
                }
            }
            $employee->dniType_id = $request['dniType_id'];
            $employee->area_id = $request['area_id'];
            $employee->country_id = $request['country_id'];
            $employee->email = GeneralHelper::generateEmail($employee, $domain->domain);
            $employee->save();
            return response("Empleado actualizado exitosamente", 200);
        } catch (\Throwable $th) {
            return response("No se pudo actualizar el empleado", 403);
        }
    }

    public function destroy(Employee $employee)
    {
        
        try {
            $employee->delete();
            return response('Empleado eliminado correctamente', 200);   
        } catch (\Throwable $th) {
            return response("No se pudo eliminar el empleado");
        }
        
    }

    public function getCountries()
    {
        return Country::all();
    }

    public function getTypes()
    {
        return DniType::all();
    }

    public function getAreas()
    {
        return Area::all();
    }

    
}
