<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'surname' => 'max:20|required|regex:/^[A-Z\s]+$/',
            'first_name' => 'max:20|required|regex:/^[A-Z\s]+$/',
            'second_surname' => 'max:20|required|regex:/^[A-Z\s]+$/',
            'other_name' => 'max:50|regex:/^[A-Z\s]+$/',
            'country_id' => 'required',
            'dniType_id' => 'required',
            'dni_number' => 'max:20|regex:/^[A-Za-z0-9]+$/',
            'area_id' => 'required'
        ];
    }

    public function attributes()
    {
        return [
            'surname' => 'Primer apellido',
            'first_name' => 'Primer nombre',
            'second_surname' => 'Segundo apellido',
            'other_name' => 'Otros nombres',
            'date_of_admission' => 'Fecha de ingreso',
            'dni_number' => 'Número de identificación',
            'dniType_id' => 'Tipo de identificación',
            'area_id' => 'Área de trabajo',
            'country_id' => 'País'
        ];
    }

    public function messages()
    {
        return [
            "max" => "El campo :attribute debe contener max 20 caracteres",
            'surname.regex' => 'El campo :attribute debe ser en mayusculas, no debe contener caracteres especiales ni números',
            'required' => 'El campo :attribute es requerido',
            'dni_number.regex' => 'El campo :attribute debe ser alfanumerico sin espacios',
            'unique' => 'El campo :attribute ya está registrado en el sistema'
        ];
    }
}
