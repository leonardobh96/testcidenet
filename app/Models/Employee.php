<?php

namespace App\Models;

use App\Models\Area;
use App\Models\Country;
use App\Models\DniType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Employee extends Model
{
    use HasFactory;

    protected $fillable = [
        'surname',
        'second_surname',
        'first_name',
        'other_name',
        'email',
        'date_of_admission',
        'state',
        'dni_number',
        'dniType_id',
        'area_id',
        'country_id'
    ];

    public function country(){
        return $this->belongsTo(Country::class);
    }

    public function area(){
        return $this->belongsTo(Area::class);
    }

    public function dniType(){
        return $this->belongsToMany(DniType::class, 'dniType_id');
    }

    public function setDateOfAdmissionttribute($value)
    {
        $this->attributes['date_of_admission'] = $value->locale('en')->isoFormat('dddd, MMMM Do YYYY, h:mm');
    }
}
